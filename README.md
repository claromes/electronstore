# Store Management App

Desktop app exemple made with Electron, Photon, Vue.js and LokiJS following [this playlist](https://www.youtube.com/playlist?list=PLFg79aDx1Frvf0L58PV5mAga28lVphw5f).

## Dev tricks

### Installation

To install Electron globally use `npm install -g electron --unsafe-perm=true --allow-root` and avoid some errors.

### Node integration

By default `nodeIntegration` is false in Electron 5.0.0.<br>
In main.js file find the function `createWindow()` and add a new property in `webPreferences` object: `nodeIntegration: true`.

### LokiJS documentation

The complete documentation can be found [here](https://techfort.github.io/LokiJS).
