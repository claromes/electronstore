var loki = require("lokijs");
var db = new loki("db.json");
var clients = db.addCollection("clients");

function ready(fn) {
  if (document.readyState != "loading") {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

ready(function() {
  document.querySelector("#save").addEventListener("click", function(e) {
    e.preventDefault();
    let data = {
      name: document.querySelector("#name").value,
      email: document.querySelector("#email").value
    };
    clients.insert(data);
    db.save();
    // Verify if the data is saved and return a message error
    alert("Saved!");
    document.querySelector("#form").reset();
  });
});
