var read = require("read-file-utf8");
var data = read(__dirname + "/db.json");

var loki = require("lokijs");
var db = new loki("db.json");

db.loadJSON(data);

//Listing data
window.vue = require("vue");
var clients = db.getCollection("clients");

new Vue({
  el: "#listClient",
  data: {
    clients: []
  },
  mounted: function() {
    this.clients = clients.data;
    console.log(this.clients);
  }
});
